import React, { useEffect, useState, useCallback } from "react";
import { dataServ } from "./services/services";

export default function QuanLySanPham() {
  let [danhSachSP, setDanhSachSP] = useState([]);
  let [errorValidate, setErrorValidate] = useState({
    id: "",
    name: "",
    img: "",
    desc: "",
    price: "",
  });
  let [thongTinSP, setThongTinSP] = useState({
    id: "",
    name: "",
    img: "",
    desc: "",
    price: "",
  });
  let [isValidThemMoiSP, setIsValidThemMoiSP] = useState(false);

  let [danhSachTimKiem, setDanhSachTimKiem] = useState([...danhSachSP]);

  const handleLuuThongTinSP = (e) => {
    let tagInput = e.target;
    let { value, name } = tagInput;
    let errorMessage = "";
    value.trim() === "" &&
      name === "id" &&
      (errorMessage = "Mã sản phẩm không được để trống");
    value.trim() === "" &&
      name === "name" &&
      (errorMessage = "Tên sản phẩm không được để trống");
    value.trim() === "" &&
      name === "img" &&
      (errorMessage = "Hình ảnhh sản phẩm không được để trống");
    value.trim() === "" &&
      name === "desc" &&
      (errorMessage = "Mô tả sản phẩm không được để trống");
    value.trim() === "" &&
      name === "price" &&
      (errorMessage = "Giá sản phẩm không được để trống");

    if (name === "img") {
      let regex = /^(ftp|http|https):\/\/[^ "]+$/;
      if (!regex.test(value)) {
        errorMessage = "Không đúng định dạng";
      }
    }

    setThongTinSP({ ...thongTinSP, [name]: [value] });

    setErrorValidate({ ...errorValidate, [name]: errorMessage });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (isValidThemMoiSP) {
      setDanhSachSP([...danhSachSP, thongTinSP]);
    }
  };

  const handleXoaSP = (sanPham) => {
    let index = danhSachSP.findIndex((item) => {
      return sanPham.id === item.id;
    });
    let danhSachSPUpdated = [...danhSachSP];
    danhSachSPUpdated.splice(index, 1);
    setDanhSachSP(danhSachSPUpdated);
  };

  const handleSuaSP = (sanPham) => {
    setThongTinSP({
      ...thongTinSP,
      id: sanPham.id,
      name: sanPham.name,
      img: sanPham.img,
      desc: sanPham.desc,
      price: sanPham.price,
    });
  };

  const resetInput = () => {
    setThongTinSP({
      ...thongTinSP,
      id: "",
      name: "",
      img: "",
      desc: "",
      price: "",
    });
  };

  const thongTinTimKiem = (e) => {
    let idTimKiem = e.target.value;

    let index = danhSachSP.findIndex((item) => {
      return item.id == idTimKiem;
    });
    setDanhSachTimKiem([...danhSachTimKiem, danhSachSP[index]]);
  };

  const handleTimKiemSP = () => {
    setDanhSachSP(danhSachTimKiem);
  };

  useEffect(() => {
    dataServ
      .getDataServ()
      .then((res) => {
        setDanhSachSP(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  useEffect(() => {
    let isValid = true;
    for (let key in thongTinSP) {
      if (thongTinSP[key] === "" || errorValidate[key] !== "") {
        isValid = false;
      }
    }
    setIsValidThemMoiSP(isValid);
  }, [thongTinSP]);

  return (
    <div className="container-fluid">
      <h3 className="display-4 text-center">Quản Lý Sản Phẩm</h3>
      <div className="row">
        <div className="col-8 mx-auto">
          <div className="row">
            <div className="col-6 text-left">
              <button
                id="btnThemSP"
                class="btn btn-success"
                data-toggle="modal"
                data-target="#myModal"
                onClick={resetInput}
              >
                <i class="fa fa-plus mr-1"></i>
                Thêm Mới
              </button>
            </div>
            <div className="col-6">
              <div className="input-group mb-3">
                <input
                  id="inputTK"
                  type="text"
                  className="form-control"
                  placeholder="Nhập từ khóa"
                  aria-label="Recipient's username"
                  aria-describedby="basic-addon2"
                  onChange={thongTinTimKiem}
                />
                <div className="input-group-append">
                  <span
                    className="input-group-text"
                    id="basic-addon2"
                    onClick={handleTimKiemSP}
                  >
                    <i className="fa fa-search" />
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="myModal" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            {/* Modal Header */}
            <div className="modal-header">
              <h3 className="modal-title">Thông tin sản phẩm</h3>
              <button type="button" className="close" data-dismiss="modal">
                ×
              </button>
            </div>
            {/* Modal body */}
            <div className="modal-body text-left">
              <form onSubmit={handleSubmit}>
                <div className="form-group">
                  <label style={{ fontSize: "16px" }}>Mã Sản Phẩm</label>
                  <input
                    type="number"
                    id="id"
                    name="id"
                    className="form-control"
                    placeholder="Nhập vào tên sản phẩm"
                    value={thongTinSP.id}
                    onChange={handleLuuThongTinSP}
                  />

                  <span className="text-danger">{errorValidate.id}</span>
                </div>
                <div className="form-group">
                  <label style={{ fontSize: "16px" }}>Tên Sản Phẩm</label>
                  <input
                    type="text"
                    id="name"
                    name="name"
                    className="form-control"
                    placeholder="Nhập vào tên sản phẩm"
                    value={thongTinSP.name}
                    onChange={handleLuuThongTinSP}
                  />
                  <span className="text-danger">{errorValidate.name}</span>
                </div>

                <div className="form-group">
                  <label style={{ fontSize: "16px" }}>Hình Ảnh</label>
                  <input
                    type="text"
                    id="img"
                    name="img"
                    className="form-control"
                    placeholder="Nhập vào hình sản phẩm"
                    value={thongTinSP.img}
                    onChange={handleLuuThongTinSP}
                  />
                  <span className="text-danger">{errorValidate.img}</span>
                </div>
                <div className="form-group">
                  <label style={{ fontSize: "16px" }}>Mô tả</label>
                  <textarea
                    type="text"
                    id="desc"
                    className="form-control"
                    name="desc"
                    cols={30}
                    rows={10}
                    placeholder="Nhập vào mô tả"
                    defaultValue={""}
                    value={thongTinSP.desc}
                    onChange={handleLuuThongTinSP}
                  />
                  <span className="text-danger">{errorValidate.desc}</span>
                </div>
                <div className="form-group">
                  <label style={{ fontSize: "16px" }}>Giá</label>
                  <input
                    id="price"
                    name="price"
                    type="number"
                    className="form-control"
                    placeholder="Nhập vào giá"
                    value={thongTinSP.price}
                    onChange={handleLuuThongTinSP}
                  />
                  <span className="text-danger">{errorValidate.price}</span>
                </div>
                {/* Modal footer */}
                <div className="modal-footer">
                  {isValidThemMoiSP ? (
                    <button
                      type="submit"
                      className="btn btn-success"
                      // data-dismiss="modal"
                    >
                      Thêm sản phẩm
                    </button>
                  ) : (
                    <button type="submit" className="btn btn-success" disabled>
                      Thêm sản phẩm
                    </button>
                  )}
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-8 mx-auto">
          <table className="table">
            <thead>
              <tr>
                <th>Mã </th>
                <th>Tên </th>
                <th>Hình ảnh</th>
                <th>Mô tả</th>
                <th>Giá</th>
                <th>Thao tác</th>
              </tr>
            </thead>
            <tbody>
              {danhSachSP.map((item) => {
                return (
                  <tr>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>
                      <img src={item.img} alt="" style={{ width: "40px" }} />
                    </td>
                    <td>{item.desc}</td>
                    <td>{item.price}</td>
                    <td>
                      <button
                        className="btn btn-danger mr-3"
                        onClick={() => {
                          handleXoaSP(item);
                        }}
                      >
                        Xoá
                      </button>
                      <button
                        className="btn btn-primary"
                        data-toggle="modal"
                        data-target="#myModal"
                        onClick={() => {
                          handleSuaSP(item);
                        }}
                      >
                        Sửa
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
