import { dataServ } from "./PhoneCart/services/services";

export const dataAxios = () => {
  dataServ
    .getDataServ()
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      console.log(err);
    });
};
