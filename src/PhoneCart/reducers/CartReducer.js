import {
  ON_CANCEL_MODAL,
  ON_OK_MODAL,
  ON_PURCHASE,
  TANG_GIAM,
  THEM_GIO_HANG,
  XOA_SP,
} from "../constants/constants";
import { localStorageServ } from "../services/services";

let initialState = {
  gioHang: localStorageServ.getGioHang() || [],
  soLuongSPGioHang: localStorageServ.getSoLuongSPGioHang() || 0,
  isModalVisible: false,
  tongTien: localStorageServ.getTongTien() || 0,
};

export const CartReducer = (state = initialState, action) => {
  switch (action.type) {
    case THEM_GIO_HANG: {
      let gioHangUpdated = [...state.gioHang];

      let index = gioHangUpdated.findIndex((item) => {
        return action.sanPham.id === item.id;
      });
      if (index === -1) {
        let sanPham = { ...action.sanPham, soLuong: 1 };
        gioHangUpdated.push(sanPham);
        state.tongTien += sanPham.price * 1;
      } else {
        gioHangUpdated[index].soLuong += 1;
        state.tongTien += gioHangUpdated[index].price * 1;
      }

      state.soLuongSPGioHang += 1;

      state.gioHang = gioHangUpdated;

      localStorageServ.setGioHang(state.gioHang);

      localStorageServ.setSoLuongSPGioHang(state.soLuongSPGioHang);

      localStorageServ.setTongTien(state.tongTien);

      return { ...state };
    }
    case XOA_SP: {
      let gioHangUpdated = [...state.gioHang];

      let index = gioHangUpdated.findIndex((item) => {
        return action.sanPham.id === item.id;
      });

      state.soLuongSPGioHang += gioHangUpdated[index].soLuong * -1;
      state.tongTien +=
        gioHangUpdated[index].price * gioHangUpdated[index].soLuong * -1;
      gioHangUpdated.splice(index, 1);
      state.gioHang = gioHangUpdated;
      localStorageServ.setGioHang(state.gioHang);

      localStorageServ.setSoLuongSPGioHang(state.soLuongSPGioHang);

      localStorageServ.setTongTien(state.tongTien);
      return { ...state };
    }
    case TANG_GIAM: {
      let gioHangUpdated = [...state.gioHang];
      let index = gioHangUpdated.findIndex((item) => {
        return item.id === action.sanPham.id;
      });
      gioHangUpdated[index].soLuong += action.giaTri;
      state.soLuongSPGioHang += action.giaTri;
      state.tongTien += gioHangUpdated[index].price * action.giaTri * 1;

      gioHangUpdated[index].soLuong === 0 && gioHangUpdated.splice(index, 1);
      gioHangUpdated.length === 0 && (state.isModalVisible = false);
      state.gioHang = gioHangUpdated;
      localStorageServ.setGioHang(state.gioHang);

      localStorageServ.setSoLuongSPGioHang(state.soLuongSPGioHang);

      localStorageServ.setTongTien(state.tongTien);
      return { ...state };
    }
    case ON_OK_MODAL: {
      state.isModalVisible = true;

      return { ...state };
    }
    case ON_CANCEL_MODAL: {
      state.isModalVisible = false;
      return { ...state };
    }
    case ON_PURCHASE: {
      state.isModalVisible = false;
      let gioHangUpdated = [...state.gioHang];
      gioHangUpdated = [];
      state.gioHang = gioHangUpdated;
      state.soLuongSPGioHang = 0;
      state.tongTien = 0;
      localStorageServ.setGioHang(state.gioHang);
      localStorageServ.setSoLuongSPGioHang(state.soLuongSPGioHang);
      localStorageServ.setTongTien(state.tongTien);
      return { ...state };
    }
    default:
      return state;
  }
};
