import { combineReducers } from "redux";
import { CartReducer } from "./CartReducer";

export const root = combineReducers({ CartReducer });
