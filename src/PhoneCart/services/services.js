import axios from "axios";

export const dataServ = {
  getDataServ: () => {
    return axios({
      method: "GET",
      url: `https://62907aa9665ea71fe1331c72.mockapi.io/Products`,
    });
  },
};

export const localStorageServ = {
  setGioHang: (gioHang) => {
    let dataJSON = JSON.stringify(gioHang);
    localStorage.setItem("gioHang", dataJSON);
  },
  getGioHang: () => {
    let dataGioHang = localStorage.getItem("gioHang");
    return JSON.parse(dataGioHang);
  },
  setSoLuongSPGioHang: (soLuong) => {
    let dataJSON = JSON.stringify(soLuong);
    localStorage.setItem("soLuong", dataJSON);
  },
  getSoLuongSPGioHang: () => {
    let soLuong = localStorage.getItem("soLuong");
    return JSON.parse(soLuong);
  },
  setTongTien: (tongTien) => {
    let dataJSON = JSON.stringify(tongTien);
    localStorage.setItem("tongTien", dataJSON);
  },
  getTongTien: () => {
    let tongTien = localStorage.getItem("tongTien");
    return JSON.parse(tongTien);
  },
};
