import { Button, Modal } from "antd";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  ON_CANCEL_MODAL,
  ON_OK_MODAL,
  ON_PURCHASE,
  TANG_GIAM,
  XOA_SP,
} from "./constants/constants";
import { localStorageServ } from "./services/services";

export default function ModalConfirm({}) {
  let gioHang = localStorageServ.getGioHang() || [];

  let tongTien = useSelector((state) => {
    return state.CartReducer.tongTien;
  });

  let isModalVisible = useSelector((state) => {
    return state.CartReducer.isModalVisible;
  });

  const dispatch = useDispatch();

  let tangGiamSoLuongSP = (sanPham, giaTri) => {
    dispatch({ type: TANG_GIAM, sanPham, giaTri });
  };

  let xoaSP = (sanPham) => {
    dispatch({ type: XOA_SP, sanPham });
  };

  let onOkModal = () => {
    dispatch({ type: ON_OK_MODAL });
  };

  let onCancelModal = () => {
    dispatch({ type: ON_CANCEL_MODAL });
  };

  let onPurchase = () => {
    dispatch({ type: ON_PURCHASE });
  };
  return (
    <>
      <Modal
        title="Thông tin giỏ hàng"
        visible={isModalVisible}
        onOk={onOkModal}
        onCancel={onCancelModal}
        style={{ minWidth: "1000px" }}
        footer={[
          <Button key="submit" type="primary" onClick={onPurchase}>
            Thanh toán
          </Button>,
        ]}
      >
        <table class="table text-center">
          <thead>
            <tr>
              <th scope="col">Mã sản phẩm</th>
              <th scope="col">Hình ảnh</th>
              <th scope="col">Tên</th>
              <th scope="col">Số lượng</th>
              <th scope="col">Giá</th>
              <th scope="col">Tổng tiền</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {gioHang.map((item) => {
              return (
                <tr>
                  <td>{item.id}</td>
                  <td>
                    <img style={{ width: "40px" }} src={item.img} alt="" />
                  </td>
                  <td>{item.name}</td>
                  <td>
                    <div
                      class="btn-group mr-2"
                      role="group"
                      aria-label="Second group"
                    >
                      <button
                        type="button"
                        class="btn btn-light"
                        onClick={() => {
                          tangGiamSoLuongSP(item, 1);
                        }}
                      >
                        +
                      </button>
                      <span className="btn">{item.soLuong}</span>
                      <button
                        type="button"
                        class="btn btn-light"
                        onClick={() => {
                          tangGiamSoLuongSP(item, -1);
                        }}
                      >
                        -
                      </button>
                    </div>
                  </td>
                  <td>${item.price}</td>
                  <td>${item.price * item.soLuong}</td>
                  <td>
                    {" "}
                    <button
                      className="btn btn-danger"
                      onClick={() => {
                        xoaSP(item);
                      }}
                    >
                      Xoá
                    </button>
                  </td>
                </tr>
              );
            })}
            <tr>
              <td colSpan={7} className="text-right font-weight-bold ">
                Tiền thanh toán: ${tongTien}
              </td>
            </tr>
          </tbody>
        </table>
      </Modal>
    </>
  );
}
