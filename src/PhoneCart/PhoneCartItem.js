import React from "react";
import { useDispatch } from "react-redux";
import { THEM_GIO_HANG } from "./constants/constants";
import "../styles/styles.css";

export default function PhoneCartItem({ item, key }) {
  let dispatch = useDispatch();

  const dispatchAddToCart = (sanPham) => {
    dispatch({ type: THEM_GIO_HANG, sanPham });
  };
  return (
    <div
      key={key}
      className=" card"
      style={{
        width: "15rem ",
        objectFit: "cover",
        backgroundColor: "#fff",
        border: "none",
        borderRadius: "30px",
        paddingTop: "20px",
        overflow: "hidden",
      }}
    >
      <img
        style={{ width: "100%", borderRadius: "24px", position: "relative" }}
        className="card-img-top"
        src={item.img}
        alt="Card image cap"
      />
      <div className="card-body text-center">
        <h4 style={{ minHeight: "60px" }} className="card-title">
          {item.name}
        </h4>

        <h5 className="text-right">Giá: ${item.price}</h5>
        <a
          onClick={() => {
            dispatchAddToCart(item);
          }}
          className="btn btn-primary"
        >
          Thêm vào giỏ hàng
        </a>
      </div>
      <div className="inside">
        <div className="icon">
          <i class="fa fa-info-circle"></i>
        </div>
        <div className="contents ">
          <h4 className="text-white">Thông tin sản phẩm</h4>
          <p>Màn hình: {item.screen}</p>
          <p>Camera sau: {item.backCamera}</p>
          <p>Cam trước: {item.frontCamera}</p>
        </div>
      </div>
    </div>
  );
}
