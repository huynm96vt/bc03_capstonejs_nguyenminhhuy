import React, { useEffect, useState } from "react";
import PhoneCartItem from "./PhoneCartItem";
import { dataServ, localStorageServ } from "./services/services";
import ModalConfirm from "./ModalConfirm";
import { useDispatch, useSelector } from "react-redux";
import { ON_OK_MODAL } from "./constants/constants";

export default function PhoneCart() {
  let [data, setData] = useState([]);
  let [dataFilter, setDataFilter] = useState([]);
  let soLuongSPGioHang = useSelector((state) => {
    return state.CartReducer.soLuongSPGioHang;
  });

  const renderData = () => {
    return dataFilter.map((item, index) => {
      return <PhoneCartItem item={item} key={index} />;
    });
  };

  useEffect(() => {
    dataServ
      .getDataServ()
      .then((res) => {
        setData(res.data);
        setDataFilter(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const filterType = (e) => {
    let type = e.target.value;
    let cloneDataFilter = [];
    data.forEach((item) => {
      type === item.type && cloneDataFilter.push(item);
    });
    setDataFilter(cloneDataFilter);
    type === "default" && setDataFilter(data);
  };

  const dispatch = useDispatch();

  const showModal = () => {
    dispatch({ type: ON_OK_MODAL });
  };

  return (
    <div
      style={{
        background: "linear-gradient(to right, #bdc3c7, #2c3e50)",
        width: "100%",
        height: "100%",
      }}
    >
      <ModalConfirm />
      <div
        className=" container text-right "
        style={{ width: "100%", height: "30px" }}
      >
        <span
          onClick={showModal}
          style={{ fontSize: "30px ", cursor: "pointer" }}
        >
          <i class="fa fa-shopping-cart"></i>

          <span
            style={{
              fontSize: "24px",
              color: "red",
              position: "relative",
              bottom: "10px",
            }}
          >
            {soLuongSPGioHang}
          </span>
        </span>
      </div>
      <div className="container my-4">
        <select class="custom-select" onChange={filterType}>
          <option value="default" selected>
            Chọn loại điện thoại
          </option>
          <option value="Samsung">SamSung</option>
          <option value="Iphone">Iphone</option>
        </select>
      </div>
      <div className=" py-5 row justify-content-around">{renderData(data)}</div>
    </div>
  );
}
